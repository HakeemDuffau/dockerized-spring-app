FROM maven:3.9.3-eclipse-temurin-20-alpine@sha256:1b9ca6481fa3bfb34fa49279548f66c66a197090e455a8ecd51dd133ddb30a82 As build
WORKDIR /app
COPY  . .
RUN mvn clean package

FROM openjdk:17-alpine@sha256:4b6abae565492dbe9e7a894137c966a7485154238902f2f25e9dbd9784383d81
WORKDIR /app
EXPOSE 8080
RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
USER javauser
COPY --from=build --chown=javauser:javauser /app/target/user-api-0.0.1-SNAPSHOT.jar application.jar 
ENTRYPOINT [ "java", "-jar", "application.jar" ]